# 第四章 外部数据包装器和并行查询{docsify-ignore} 

 本章将描述两个技术上有趣且实用的特性：外部数据包装器(FDW)和并行查询。

## 4.1. 外部数据包装器 (FDW)

2003年，SQL标准中添加了一个访问远程数据的规范，称为[SQL管理外部数据](https://wiki.postgresql.org/wiki/SQL/MED)(SQL/MED)。PostgreSQL一直在开发此功能，从9.1版本就实现了SQL/MED的一部分。

在SQL/MED中，远程服务器上的表称为外部表。PostgreSQL的外部数据包装器(FDW)使用SQL/MED来管理与本地表类似的外部表。

**图.4.1. FDW基本概念**

![Fig. 4.1. Basic concept of FDW.](imgs/ch4/fig-4-fdw-1.png)

安装必要的扩展并进行适当的设置后，您可以访问远程服务器上的外部表。例如，假设有两个远程服务器，即，postgresql和mysql，它们分别具有foreign_pg_tbl表和foreign_my_tbl表。在此示例中，您可以通过发出SELECT查询从本地服务器访问外部表，如下所示。

```sql
localdb=# -- foreign_pg_tbl is on the remote postgresql server. 
localdb-# SELECT count(*) FROM foreign_pg_tbl;
 count 
-------
 20000

localdb=# -- foreign_my_tbl is on the remote mysql server. 
localdb-# SELECT count(*) FROM foreign_my_tbl;
 count 
-------
 10000
```

此外，您可以使用存储在不同服务器中的外部表执行联接操作，这些外部表与本地表类似。

```sql
localdb=# SELECT count(*) FROM foreign_pg_tbl AS p, foreign_my_tbl AS m WHERE p.id = m.id;
 count 
-------
 10000
```

许多FDW扩展已经开发并在[Postgres wiki](https://wiki.postgresql.org/wiki/Foreign_data_wrappers)中列出。但是，除了[postgres_fdw](https://www.postgresql.org/docs/current/static/postgres-fdw.html)之外，几乎所有扩展都没有得到适当维护，postgres_fdw由PostgreSQL全球开发组作为访问远程PostgreSQL服务器的扩展正式开发和维护。

PostgreSQL的FDW将在以下部分中详细介绍。第4.1.1节概述了PostgreSQL中的FDW。第4.1.2节描述了postgres_fdw扩展的执行方式。

> Citus
>
> [Citus](https://github.com/citusdata/citus)是由[citusdata.com](https://www.citusdata.com/)开发的开源PostgreSQL扩展，用于创建用于查询并行化的分布式服务器集群。它是PostgreSQL在机械上最复杂、商业上最成功的扩展之一，也是FDW的一种类型。

### 4.1.1. 概述

要使用FDW特性，您需要安装相应的扩展并执行设置命令，例如[CREATE FOREIGN TABLE](https://www.postgresql.org/docs/current/static/sql-createforeigntable.html), [CREATE SERVER](https://www.postgresql.org/docs/current/static/sql-createserver.html) 和 [CREATE USER MAPPING](https://www.postgresql.org/docs/current/static/sql-createusermapping.html)(相关详细信息，请参阅官方文档)。

在提供适当的设置之后，在查询处理期间调用扩展中定义的函数来访问外部表。

图4.2 简要描述了FDW在PostgreSQL中的执行方式

**图4.2 FDW的执行方式**

![Fig. 4.2. How FDWs perform.](imgs/ch4/fig-4-fdw-2.png)

(1) 分析器创建输入SQL的查询树。

(2) 规划器(或执行器)连接到远程服务器。

(3) 如果[use_remote_estimate](https://www.postgresql.org/docs/current/static/postgres-fdw.html#id-1.11.7.43.10.4)选项为ON(默认值为OFF)，则计划者执行解释命令来估计每个计划路径的成本。

(4) 计划器从计划树中创建纯文本SQL语句，内部称为 *deparesing*。

(5) 执行器将纯文本SQL语句发送到远程服务器并接收结果。

如果有必要，执行器将处理接收到的数据。例如，如果执行多表查询，则执行器执行接收数据和其他表的联接处理。

每个处理的详细信息将在下面的章节中描述。

#### 4.1.1.1. 创建查询树

分析器使用外部表的定义创建输入SQL的查询树，外部表使用[CREATE FOREIGN TABLE](https://www.postgresql.org/docs/current/static/sql-createforeigntable.html)或[IMPORT FOREIGN SCHEMA](https://www.postgresql.org/docs/current/static/sql-importforeignschema.html)命令存储在[pg_catalog.pg_class](https://www.postgresql.org/docs/current/static/catalog-pg-class.html) and[pg_catalog.pg_foreign_table](https://www.postgresql.org/docs/current/static/catalog-pg-foreign-table.html)系统表中。

#### 4.1.1.2. 连接到远程服务器

若要连接到远程服务器，计划器(或执行器)使用特定的库连接到远程数据库服务器。例如，为了连接到远程PostgreSQL服务器，postgres_fdw使用[libpq](https://www.postgresql.org/docs/current/static/libpq.html)。为了连接到MySQL服务器，由EnterpriseDB开发的[mysql_fdw](https://github.com/EnterpriseDB/mysql_fdw)使用libmysqlclient。

连接参数，如用户名、服务器的IP地址和端口号，使用[CREATE USER MAPPING](https://www.postgresql.org/docs/current/static/sql-createusermapping.html)和[CREATE SERVER](https://www.postgresql.org/docs/current/static/sql-createserver.html) 命令存储在[pg_catalog.pg_user_mapping](https://www.postgresql.org/docs/current/static/catalog-pg-user-mapping.html)和[pg_catalog.pg_foreign_server](https://www.postgresql.org/docs/current/static/catalog-pg-foreign-server.html)系统表中。

#### 4.1.1.3. 使用EXPLAIN命令创建计划树(可选)

PostgreSQL的FDW支持获取外部表的统计信息以估计查询的计划树的功能，某些FDW扩展(如postgres_fdw, mysql_fdw, tds_fdw 和 jdbc2_fdw)支持该特性。

如果使用[ALTER SERVER](https://www.postgresql.org/docs/current/static/sql-alterserver.html)命令将use_remote_estimate选项设置为ON，则计划器通过执行EXPLAIN命令查询到远程服务器的计划成本；否则，默认情况下使用嵌入的常量值。

```sql-monosp
localdb=# ALTER SERVER remote_server_name OPTIONS (use_remote_estimate 'on');
```

尽管有些扩展使用EXPLAIN命令的值，但只有postgres_fdw可以反映EXPLAIN命令的结果，因为PostgreSQL的EXPLAIN命令返回启动和总成本。

EXPLAIN命令的结果不能被其他DBMS FDW扩展用于规划。例如，MySQL的EXPLAIN命令只返回估计的行数；然而，PostgreSQL的计划器需要更多信息来估计成本，如[第3章](ch3.md)所述。

#### 4.1.1.4. Deparesing

为了生成计划树，规划器从外部表的PLAN树的扫描路径创建纯文本SQL语句。例如，图4.3显示以下SELECT语句的计划树。

```sql
localdb=# SELECT * FROM tbl_a AS a WHERE a.id < 10;
```

图4.3显示了从PlannedStmt的计划树链接的ForeignScan节点存储了一个普通的SELECT文本。在这里，postgres_fdw从通过解析和分析创建的查询树中重新创建一个简单的SELECT文本，这在PostgreSQL中被称为deparsing。

**图4.3 扫描外部表的计划树示例**

![Fig. 4.3. Example of the plan tree that scans a foreign table.](imgs/ch4/fig-4-fdw-3.png)

mysql_fdw的使用从查询树中为MySQL重新创建一个SELECT文本。[redis_fdw](https://github.com/pg-redis-fdw/redis_fdw)或[rw_redis_fdw](https://github.com/nahanni/rw_redis_fdw)的使用将创建一个[SELECT命令](https://redis.io/commands/select)。

#### 4.1.1.5. 发送SQL语句和接收结果

deparsing后，执行器将deparsed的SQL语句发送到远程服务器并接收结果。

将SQL语句发送到远程服务器的方法取决于每个扩展的开发人员。例如，mysql_fdw发送SQL语句而不使用事务。在mysql_fdw中执行SELECT查询的典型SQL语句序列如下所示(图4.4)。

(5-1) 将SQL_MODE设置为‘ANSI_LOOTS’。

(5-2) 向远程服务器发送SELECT语句。

(5-3) 从远程服务器接收结果。

在这里，mysql_fdw将结果转换为PostgreSQL提供的可读数据。

所有FDW扩展都实现了将结果转换为PostgreSQL可读数据的功能。

**图4.4 在mysql_fdw中执行SELECT查询的典型SQL语句序列**  

![Fig. 4.4. Typical sequence of SQL statements to execute a SELECT query in mysql_fdw](imgs/ch4/fig-4-fdw-4.png)

可以在这里找到远程服务器的实际日志；显示远程服务器接收到的语句。

在postgres_fdw中，SQL命令的顺序是复杂的。在postgres_fdw中执行SELECT查询的典型SQL语句序列如下所示(图4.5)。

(5-1) 启动远程事务。

默认的远程事务隔离级别是Repeable Read；如果本地事务的隔离级别设置为SERIALIZABLE，则远程事务也设置为SERIALIZABLE。

(5-2)-(5-4) 声明一个游标。

SQL语句基本上是作为游标执行的。

(5-5) 执行FETCH命令以获得结果。

默认情况下，FETCH命令获取100行数据。

(5-6) 从远程服务器接收结果。

(5-7) 关闭光标。

(5-8) 提交远程事务。

**图 4.5 在postgres_fdw中执行SELECT查询的典型SQL语句序列**

![Fig. 4.5. Typical sequence of SQL statements to execute a SELECT query in postgres_fdw.](imgs/ch4/fig-4-fdw-5.png)

远程服务器的实际日志可以在这里找到。

> postgres_fdw中默认的远程事务隔离级别。
>
> 对于为什么默认的远程事务隔离级别是可重复读取的，在[官方文档](https://www.postgresql.org/docs/10/static/postgres-fdw.html#id-1.11.7.43.12)中提供了解释。
>
> 当本地事务具有SERIALIZABLE隔离级别时，远程事务使用SERIALIZABLE隔离级别；否则，它使用可重复读取隔离级别。此选择确保了如果查询在远程服务器上执行多个表扫描，它将获得所有扫描的快照一致的结果。结果是，单个事务中的连续查询将看到来自远程服务器的相同数据，即使由于其他活动而在远程服务器上发生并发更新。

### 4.1.2. Postgres_fdw扩展如何执行

postgres_fdw扩展是由PostgreSQL全球开发小组正式维护的一个特殊模块，其源代码包含在PostgreSQL源代码中。

postgres_fdw逐步改善。表4.1列出官方文档中与postgres_fdw有关的发行说明。

| 版本 | 描述                                                         |
| ---- | ------------------------------------------------------------ |
| 9.3  | postgres_fdw module is released.                             |
| 9.6  | Consider performing sorts on the remote server.Consider performing joins on the remote server.When feasible, perform UPDATE or DELETE entirely on the remote server.Allow the fetch size to be set as a server or table option. |
| 10   | Push aggregate functions to the remote server, when possible. |

鉴于上一节描述postgres_fdw如何处理单表查询，下面的小节介绍postgres_fdw如何处理多表查询、排序操作和聚合函数。

这个小节关注SELECT语句；然而，postgres_fdw也可以处理其他DML(INSERT、UPDATE和DELETE)语句，如下所示。



> PostgreSQL的FDW不检测死锁。
>
> postgres_fdw和FDW特性不支持分布式锁管理器和分布式死锁检测特性。因此，可以很容易地生成死锁。例如，如果Client_A更新本地表‘tbl_Local’和外部表‘tbl_Remote’，而Client_B更新‘tbl_Remote’和‘tbl_LOCAL’，则这两个事务处于死锁状态，但PostgreSQL无法检测到这两个事务。因此，无法提交这些事务。
>
> ![](imgs/ch4/fdb-4-01.jpg)



#### 4.1.2.1. 多表查询

为了执行多表查询，postgres_fdw使用单表SELECT语句获取每个外部表，然后将它们联接到本地服务器上。

在9.5版或更早版本中，即使外部表存储在同一远程服务器中，postgres_fdw也会分别获取它们并将它们联接起来。

在9.6版或更高版本中，postgres_fdw得到了改进，当外部表在同一服务器上并且[use_remote_estimate](https://www.postgresql.org/docs/current/static/postgres-fdw.html)选项为on时，可以在远程服务器上执行远程连接操作。

执行细节如下所述。

**9.5或更早版本：**

让我们来探讨PostgreSQL如何处理连接两个外部表的查询：tbl_a和tbl_b。

```sql
localdb=# SELECT * FROM tbl_a AS a, tbl_b AS b WHERE a.id = b.id AND a.id < 200;
```

查询的EXPLAIN命令的结果如下所示。

```sql
localdb=# EXPLAIN SELECT * FROM tbl_a AS a, tbl_b AS b WHERE a.id = b.id AND a.id < 200;
                                  QUERY PLAN                                  
------------------------------------------------------------------------------
 Merge Join  (cost=532.31..700.34 rows=10918 width=16)
   Merge Cond: (a.id = b.id)
   ->  Sort  (cost=200.59..202.72 rows=853 width=8)
         Sort Key: a.id
         ->  Foreign Scan on tbl_a a  (cost=100.00..159.06 rows=853 width=8)
   ->  Sort  (cost=331.72..338.12 rows=2560 width=8)
         Sort Key: b.id
         ->  Foreign Scan on tbl_b b  (cost=100.00..186.80 rows=2560 width=8)
(8 rows)
```

结果显示，执行器选择合并联接并按以下步骤处理：

第8行：执行器使用外部表扫描获取表tbl_a。

第6行：执行器对本地服务器上获取的tbl_a行进行排序。

第11行：执行器使用外部表扫描获取表tbl_b。

第9行：执行器对本地服务器上获取的tbl_b行进行排序。

第4行：执行器在本地服务器上执行合并连接操作。

下面描述了执行器如何获取行(图4.6)。

(5-1) 启动远程事务。

(5-2) 声明游标c1，其SELECT语句如下所示：  

```sql
SELECT id,data FROM public.tbl_a WHERE (id < 200)
```

(5-3) 执行FETCH命令以获得游标1的结果。

(5-4) 声明游标c2，其SELECT语句如下所示  

```sql
SELECT id,data FROM public.tbl_b
```

请注意，原来的双表查询的WHERE子句是 "tbl_a.id = tbl_b.id AND tbl_a.id < 200"; 因此，如前面所示，可以在SELECT语句中逻辑地添加WHERE子句 "tbl_b.id < 200"。然而，postgres_fdw不能执行此推断；因此，执行器必须执行不包含任何WHERE子句的SELECT语句，并且必须获取外部表tbl_b的所有行。

此过程效率低下，因为必须通过网络从远程服务器读取不必要的行。此外，必须对接收到的行进行排序，以执行合并联接。

(5-5) 执行FETCH命令以获得游标2的结果。

(5-6) 关闭光标c1。

(5-7) 关闭光标c2。

(5-8) 提交事务。

**图 4.6 用于执行9.5版或更早版本中的多表查询的SQL语句序列**

![Fig. 4.6. Sequence of SQL statements to execute the Multi-Table Query in version 9.5 or earlier.](imgs/ch4/fig-4-fdw-6.png)![img](

远程服务器的实际日志可以在这里找到。

在接收到行之后，执行器对接收到的tbl_a和tbl_b行进行排序，然后对已排序的行执行合并连接操作。

**9.6及以后版本：**

如果use_remote_estimate选项为ON(默认为OFF)，postgres_fdw将发送几个EXPLAIN命令来获取与外部表相关的所有计划的成本。

为了发送EXPLAIN命令，postgres_fdw发送每个单表查询的EXPLAIN命令和SELECT语句的EXPLAIN命令来执行远程连接操作。在这个示例中，下面的七个EXPLAIN命令被发送到远程服务器，以获得每个SELECT语句的估计成本；然后，规划器选择最便宜的计划。

```sql
(1) EXPLAIN SELECT id, data FROM public.tbl_a WHERE ((id < 200))
(2) EXPLAIN SELECT id, data FROM public.tbl_b
(3) EXPLAIN SELECT id, data FROM public.tbl_a WHERE ((id < 200)) ORDER BY id ASC NULLS LAST
(4) EXPLAIN SELECT id, data FROM public.tbl_a WHERE ((((SELECT null::integer)::integer) = id)) AND ((id < 200))
(5) EXPLAIN SELECT id, data FROM public.tbl_b ORDER BY id ASC NULLS LAST
(6) EXPLAIN SELECT id, data FROM public.tbl_b WHERE ((((SELECT null::integer)::integer) = id))
(7) EXPLAIN SELECT r1.id, r1.data, r2.id, r2.data FROM (public.tbl_a r1 INNER JOIN public.tbl_b r2 ON (((r1.id = r2.id)) AND ((r1.id < 200))))
```

让我们在本地服务器上执行EXPLAIN命令，以观察计划器选择了什么计划。

```sql
localdb=# EXPLAIN SELECT * FROM tbl_a AS a, tbl_b AS b WHERE a.id = b.id AND a.id < 200;
                        QUERY PLAN                         
-----------------------------------------------------------
 Foreign Scan  (cost=134.35..244.45 rows=80 width=16)
   Relations: (public.tbl_a a) INNER JOIN (public.tbl_b b)
(2 rows)
```

结果表明，规划器选择在远程服务器上处理的内部连接查询是非常有效的。

下面描述了postgres_fdw是如何执行的(图4.7)。

(3-1) 启动远程事务。

(3-2) 执行EXPLAIN命令来估计每个计划路径的成本。

在本例中，执行了七个EXPLAIN命令。然后，计划者使用执行的EXPLAIN命令的结果选择SELECT查询的最便宜的成本。

(5-1) 声明游标c1，其SELECT语句如下所示：  

```sql-monosp
  SELECT r1.id, r1.data, r2.id, r2.data FROM (public.tbl_a r1 INNER JOIN public.tbl_b r2 
    ON (((r1.id = r2.id)) AND ((r1.id < 200))))
```

(5-2)从远程服务器接收结果。

(5-3)关闭光标C1。

(5-4)提交事务。



**图 4.7 在9.6版或更高版本中执行远程连接操作的SQL语句序列**

![Fig. 4.7. Sequence of SQL statements to execute the remote-join operation in version 9.6 or later.](imgs/ch4/fig-4-fdw-7.png)

远程服务器的实际日志可以在这里找到。

请注意，如果use_remote_estimate选项是off(默认情况下)，则很少选择远程连接查询，因为成本是使用非常大的嵌入式值估算的。

#### 4.1.2.2. 排序操作

在9.5版或更早版本中，排序操作(如ORDER BY)在本地服务器上处理，即本地服务器在排序操作之前从远程服务器获取所有目标行。让我们研究一下如何使用EXPLAIN命令处理包含ORDER BY子句的简单查询。

```sql
localdb=# EXPLAIN SELECT * FROM tbl_a AS a WHERE a.id < 200 ORDER BY a.id;
                              QUERY PLAN                               
-----------------------------------------------------------------------
 Sort  (cost=200.59..202.72 rows=853 width=8)
   Sort Key: id
   ->  Foreign Scan on tbl_a a  (cost=100.00..159.06 rows=853 width=8)
(3 rows)
```

第6行：执行器将以下查询发送到远程服务器，然后获取查询结果。

```sql
SELECT id, data FROM public.tbl_a WHERE ((id < 200))
```

第4行：执行器对本地服务器上获取的tbl_a行进行排序。

远程服务器的实际日志可以在这里找到。

在9.6或更高版本中，postgres_fdw可以在远程服务器上使用ORDER BY子句执行SELECT语句。

```sql
localdb=# EXPLAIN SELECT * FROM tbl_a AS a WHERE a.id < 200 ORDER BY a.id;
                           QUERY PLAN                            
-----------------------------------------------------------------
 Foreign Scan on tbl_a a  (cost=100.00..167.46 rows=853 width=8)
(1 row)
```

第4行：执行器将具有ORDER BY子句的以下查询发送到远程服务器，然后获取已经排序的查询结果。

```sql
SELECT id, data FROM public.tbl_a WHERE ((id < 200)) ORDER BY id ASC NULLS LAST
```

远程服务器的实际日志可以在这里找到。这种改进减少了本地服务器的工作量。

#### 4.1.2.3. 聚合函数

在9.6或更早版本中，类似于前面小节中提到的排序操作，在本地服务器上按以下步骤处理聚合函数，如AVG()和cont()。

```sql
localdb=# EXPLAIN SELECT AVG(data) FROM tbl_a AS a WHERE a.id < 200;
                              QUERY PLAN                               
-----------------------------------------------------------------------
 Aggregate  (cost=168.50..168.51 rows=1 width=4)
   ->  Foreign Scan on tbl_a a  (cost=100.00..166.06 rows=975 width=4)
(2 rows)
```

第5行：执行器将以下查询发送到远程服务器，然后获取查询结果。

```sql
SELECT id, data FROM public.tbl_a WHERE ((id < 200))
```

第4行：执行器计算本地服务器上获取的tbl_a行的平均值。

远程服务器的实际日志可以在这里找到。这个过程代价很高，因为发送大量行会消耗大量的网络流量，而且需要很长的时间。

在版本10或更高的版本中，postgres_fdw执行SELECT语句和远程服务器上的聚合函数(如果可能的话)。

```sql
localdb=# EXPLAIN SELECT AVG(data) FROM tbl_a AS a WHERE a.id < 200;
                     QUERY PLAN                      
-----------------------------------------------------
 Foreign Scan  (cost=102.44..149.03 rows=1 width=32)
   Relations: Aggregate on (public.tbl_a a)
(2 rows)
```

第4行：执行器将包含AVG()函数的以下查询发送到远程服务器，然后获取查询结果。

```sql
SELECT avg(data) FROM public.tbl_a WHERE ((id < 200))
```

远程服务器的实际日志可以在这里找到。这个过程显然是有效的，因为远程服务器计算平均值，并且只发送一行作为结果。

> *Push-down*
>
> 与给定的示例类似，push-down操作是本地服务器允许远程服务器处理某些操作(如聚合过程)的操作。

## 4.2. 并行查询

![](imgs/ch4/udc1.jpg)

