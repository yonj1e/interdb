### PostgreSQL的内部结构

##### 适用于数据库管理员和系统开发人员

![Copyright Diego Zazzeo](./docs/imgs/ch0/puestas-fauna-mecanica-c.png)

------

- 作者： [Hironobu SUZUKI](http://www.interdb.jp/)
- 原书名称：[《The Internals of PostgreSQL》](http://www.interdb.jp/pg/index.html)
- 译者：[杨杰](https://yonj1e.github.io/) （[yonj1e@163.com](mailto:yonj1e@163.com) ）
- 译文地址：[PostgreSQL的内部结构](https://yonj1e.gitlab.io/interdb/)

#### 法律声明

译者纯粹出于学习目的与个人兴趣翻译本书。

本译文只供学习研究参考之用，不得公开传播。

作者没有授权翻译，不要随意传播，谢谢！

#### 翻译计划

工作业余时间翻译学习，无聊一起造作啊~

![wechat](./docs/imgs/ch0/me.jpg)

#### LICENSE

CC-BY 4.0