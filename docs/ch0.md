## PostgreSQL的内部结构{docsify-ignore-all} 

#### 适用于数据库管理员和系统开发人员 

![Copyright Diego Zazzeo](/imgs/ch0/puestas-fauna-mecanica-c.png)

#### 简介

在本文档中，针对数据库管理员和系统开发人员描述了PostgreSQL的内部结构。

PostgreSQL是一个开源的多用途关系型数据库系统，在世界各地广泛使用。它是一个集成多个子系统的庞大系统，每个子系统都有其独特的复杂特征，互相协同工作。尽管理解内部机制对于使用PostgreSQL进行管理和集成至关重要，但其庞大性和复杂性决定了其难度。本文档的主要目的是解释每个子系统是如何工作的，并且描述PostgreSQL的全貌。

- [第一章. 数据库集群、数据库和表](ch1.md) 
- [第二章. 进程和内存结构](ch2.md) 
- [第三章. 查询处理](ch3.md) 
- [第四章. 外部数据包装器和并行查询](ch4.md)
- [第五章. 并发控制](ch5.md) 
- [第六章. VACUUM处理](ch6.md)
- [第七章. HOT技术和仅索引扫描](ch7.md) 
- [第八章. 缓冲区管理器](ch8.md) 
- [第九章. 预写式日志](ch9.md) 
- [第十章. 基础备份和时间点恢复](ch10.md) 
- [第十一章. 流复制](ch11.md) 

#### 作者

[Hironobu SUZUKI](http://www.interdb.jp/blog/)

#### 版权

© Copyright ALL Right Reserved, Hironobu SUZUKI.

如果您想使用本文档的任何部分和/或任何图形，请与我联系。例外：教育机构可以自由使用该文件。
